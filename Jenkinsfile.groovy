pipeline {
    agent any
    parameters {
        string(name: 'VERSION', defaultValue: "latest", description: 'A version to use')
        string(name: 'ENV_NAME', defaultValue: 'CD1', description: 'Environment name')
        string(name: 'STAGES', defaultValue: 'Build, Publish, Deploy, Test', description: 'Stages Pipeline allowed to run, values are : Build, Publish, Test, Deploy')
    }
    stages {
        stage('Init') {
            steps {
                echo "Init"

                script {
                    v = ("${VERSION}" == "latest") ? "cd-${env.BRANCH_NAME}-${env.BUILD_NUMBER}" : "${VERSION}"
                    imageName = "cdcourse/image-gallery:${v}"
                }

                echo "Version: ${v}"
                echo "Image: ${imageName}"

                script {
                    sh '''
                    git log -1 --pretty=%B | sed -e ':a' -e 'N' -e '$!ba' -e 's/\\n/ /g\' > /tmp/git_commit_info.tmp
                    '''

                    commitInfo = readFile '/tmp/git_commit_info.tmp'
                    echo "Commit info: ${commitInfo}"
                    sh '''
                    cat /tmp/git_commit_info.tmp
                    '''
                }

                slackSend message: "Good Day Sir! Started - ${env.JOB_NAME} ${env.BUILD_NUMBER}, version: ${v} (<${env.BUILD_URL}|Open>) - Running with parameters : ${params}"
                slackSend message: "Commit info: ${commitInfo}"
            }
        }

        stage('Build') {
            when {
                expression { return params.STAGES =~ /(?i)(Build)/ }
            }
            steps {
                slackSend color: "good", message: "Building - ${v}, hope it all goes well..."

                echo 'Building...'

                sh "gradle buildDocker -PimageVersion=${v}"
            }
            post {
                success { slackSend color: "good", message: "Build - ${v} is finished" }
                failure { slackSend color: "danger", message: "Build - ${v} (<${env.BUILD_URL}|Failed>)" }
            }
        }

        stage('Publish') {
            when {
                expression { return params.STAGES =~ /(?i)(Publish)/ }
            }
            steps {
                slackSend color: "good", message: "Publishing - ${v}..."
                echo 'Publishing docker image...'

                withCredentials([usernamePassword(credentialsId: 'quay-repo', usernameVariable: 'USERNAME', passwordVariable: 'PASSWORD')]) {
                    sh 'docker login -u="$USERNAME" -p="$PASSWORD" quay.io'
                    sh "docker tag ${imageName} quay.io/${imageName}"
                    sh "docker push quay.io/${imageName}"
                }
            }
            post {
                success { slackSend color: "good", message: "Published - ${v}" }
                failure { slackSend color: "danger", message: "Publish - ${v} (<${env.BUILD_URL}|Failed>)" }
            }
        }

        stage('Deploy') {
            when {
                expression { return params.STAGES =~ /(?i)(Deploy)/ }
            }
            steps {
                echo "Deploying ${v}..."
                slackSend color: "good", message: "Deploying - ${v}..."

                sh "VERSION=${v}; docker-compose up -d"
            }
            post {
                success { slackSend color: "good", message: "Deployed - ${v}" }
                failure { slackSend color: "danger", message: "Deploy of - ${v} (<${env.BUILD_URL}|Failed>)" }
            }
        }

        stage('Test') {
            when {
                expression { return params.STAGES =~ /(?i)(Test)/ }
            }
            steps {
                slackSend color: "good", message: "Testing - ${v}..."
                echo 'Testing...'
                sh "wget https://api.ghostinspector.com/v1/tests/5c9694b87faed36fd5fb54a0/execute/?apiKey=4bd6a5258a07ea40bc31accf1e513ddfad235c0c"
            }
            post {
                success { slackSend color: "good", message: "Tested - ${v}" }
                failure { slackSend color: "danger", message: "Test - ${v} (<${env.BUILD_URL}|Failed>)" }
            }
        }
    }
}

