FROM node:6-alpine

RUN npm install http-server -g

WORKDIR /usr/app

COPY dist/ .

EXPOSE 8080

CMD ["http-server","."]

